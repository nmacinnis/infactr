from .parser.SentenceParser import parse_and_store
from .model.Model import Node
from logging import info

head_node_val = "in fact,"


def get_head_node():
    return Node.all().filter("val = ", head_node_val).get()


class Initialize():
    site_name = "infactr - the website of exciting facts"

    def init(self):
        if Node.all().count(2) == 0:
            info("creating head node and initial facts")
            head_node = self.make_head_node()
            parse_and_store("exclamation creates a new fact", head_node)
            parse_and_store("question searches for related facts", head_node)
            parse_and_store("x deletes a fact", head_node)
            parse_and_store("this is a tree branch", head_node)
            parse_and_store("this is a different tree branch", head_node)

    def make_head_node(self):
        node = Node()
        node.val = head_node_val
        node.put()
        return node
