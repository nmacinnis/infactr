from google.appengine.ext import db


class Node(db.Model):
    val = db.StringProperty(multiline=False)
    mom = db.SelfReference(collection_name='children')

    def __str__(self):
        return self.val

    def tree(self):
        '''Return this tree as nested lists'''
        return [self] + [child.tree() for child in self.children]

    def grid(self):
        '''Return a once-nested list of the hierarchy'''
        self_grid = []
        for child in self.children:
            child_grid = child.grid()
            for row in child_grid:
                row.insert(0, self)
                self_grid.append(row)
        if len(self_grid) == 0:
            self_grid.append([self])
        return self_grid

    def headless_grid(self):
        '''Return a once-nested list that does not contain this node'''
        self_grid = []
        for child in self.children:
            child_grid = child.grid()
            for row in child_grid:
                self_grid.append(row)
        return self_grid

    def lineage(self):
        '''Return a list of this node's parents'''
        node = self.mom
        line = []
        while(node is not None):
            line.insert(0, node)
            node = node.mom
        return line

    def has_child(self, val):
        '''Return true if any of this node's immediate children has val val'''
        for child in self.children:
            if child.val == val:
                return True
        return False

    def get_child(self, val):
        '''Return the child of this node which has val val, or None'''
        for child in self.children:
            if child.val == val:
                return child
        return None

    def full_grid(self):
        '''Return the grid of all this node's children, including
            its lineage back to the root node'''
        grid = self.grid()
        full = []
        lineage = self.lineage()
        for row in grid:
            full.append(lineage + row)
        return full

    def count_children(self):
        '''Return the total number of children of this node, plus one'''
        return 1 + sum([child.count_children() for child in self.children])

    def length(self):
        '''Return the length of the longest path from this node to a
            leaf node, plus one'''
        return 1 + max([child.length() for child in self.children] + [0])

    def prune(self):
        '''Remove this branch from the tree if and only if it does not
            contain additional branches'''
        if self.count_children() < self.length():
            return self
        if self.val == "in fact,":
            for child in self.children:
                child.hard_prune()
            return self
        mom = db.get(self.mom.key())
        if mom.children.count() == 1:
            return mom.prune()
        else:
            self.hard_prune()
            return self.mom

    def hard_prune(self):
        '''Remove this node and all of its children from the tree'''
        for child in self.children:
            child.hard_prune()
        self.delete()

    def search(self, vals):
        '''Return the child of this node which has the first value in vals.
            If one of its children has the second value in vals, return
            that node, and so on'''
        if vals == [] or not self.has_child(vals[0]):
            return self
        else:
            return self.get_child(vals[0]).search(vals[1:])
