from .model.Model import Node
from string import punctuation


def parse_and_store(sentence, node):
    words = parse(sentence)
    words.append("!")
    cursor = node
    for word in words:
        child = cursor.get_child(word)
        if child is None:
            child = Node()
            child.val = word
            child.mom = cursor
            child.put()
        cursor = child
    return node


def parse(sentence):
    return sentence.strip(punctuation).lower().split()
