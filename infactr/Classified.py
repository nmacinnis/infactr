from .Pages import IndexPage, NodeViewPage, AdminPage, ErrorPage, RelatedNodesPage

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from .Init import Initialize

application = webapp.WSGIApplication([
    ('/', IndexPage),
    ('/node/view', NodeViewPage),
    ('/admin', AdminPage),
    ('/error', ErrorPage),
    ('/node/related', RelatedNodesPage),
],
    debug=True)


def main():
    i = Initialize()
    i.init()
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
