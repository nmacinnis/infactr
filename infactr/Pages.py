import os

from .model.Model import Node
from .parser.SentenceParser import parse_and_store, parse
from .Init import Initialize, get_head_node
from logging import info
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template


class ListPage(webapp.RequestHandler):

    ''' Base page class for our site, this handles errors and takes care of
    some basic page lifecycle stuff by providing start() and end() '''

    user = users.get_current_user()
    page_title = Initialize.site_name

    def handle_exception(self, exception, debug):
        if not debug:
            self.redirect("/error")
        else:
            webapp.RequestHandler.handle_exception(self, exception, debug)

    def start(self):
        template_values = {
            'title': self.page_title,
        }
        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/header.html')
        self.response.out.write(template.render(path, template_values))

    def end(self):

        if users.get_current_user():
            login_text = "Logged in as <i>%s</i>" % users.get_current_user(
            ).nickname()
            url = users.create_logout_url(self.request.uri)
            url_linktext = "Logout"
            admin_link = ""

            if users.is_current_user_admin():
                admin_link = self.get_admin_links()

        else:
            login_text = "Not logged in"
            url = users.create_login_url(self.request.uri)
            url_linktext = "Login"
            admin_link = ""

        template_values = {
            'login_text': login_text,
            'url': url,
            'url_linktext': url_linktext,
            'admin_link': admin_link,
        }

        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/footer.html')
        self.response.out.write(template.render(path, template_values))

    def get_admin_links(self):
        return """
            <ul>
                <li><a href="/admin">admin home</a></li>
                <li><a href="/category/admin">category admin</a></li>
                <li><a href="/post/admin">post admin</a></li>
            </ul>
            """


class AdminPage(ListPage):

    def get(self):
        self.page_title = Initialize.site_name + " &gt; Administration"
        self.start()
        self.response.out.write(
            """<div class="header"><a href="/">Jeff's List</a></div><br/>""")
        self.end()


class ErrorPage(ListPage):

    def get(self):

        template_values = {}

        self.start()
        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/error.html')
        self.response.out.write(template.render(path, template_values))
        self.end()


class IndexPage(ListPage):

    def get(self):
        node = get_head_node()
        info(node)
        template_values = {
            'node': node,
        }

        self.start()
        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/index.html')
        self.response.out.write(template.render(path, template_values))
        self.end()


class NodeViewPage(ListPage):

    def write_page(self, node):
        info("viewing node: %s" % node.val)
        template_values = {
            'node': node,
            'children': node.headless_grid(),
            'lineage': node.lineage(),
        }

        self.start()
        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/viewnode.html')
        self.response.out.write(template.render(path, template_values))
        self.end()

    def get(self):
        node_key = self.request.get('k')
        node = db.get(db.Key(node_key))
        self.write_page(node)

    def post(self):
        node_key = self.request.get('k')
        node = db.get(db.Key(node_key))
        val = self.request.get('val')
        if self.request.get('ask') is not None and self.request.get('ask') != "":
            if val == "":
                val = node.val
            page = RelatedNodesPage()
            page.request = self.request
            page.response = self.response
            page.write_page(val)
        elif self.request.get('cut') is not None and self.request.get('cut') != "":
            if val is None or val == "":
                info("cutting node: %s" % node.val)
                node = node.prune()
                self.write_page(node)
            elif val == "!":
                info("cutting node: %s" % node.val)
                leaf = node.search(["!"])
                node = leaf.prune()
                self.write_page(node)
            elif node.get_child(parse(val)[0]) is not None:
                leaf = node.search(parse(val))
                info("cutting node: %s" % leaf.val)
                node = leaf.prune()
                self.write_page(node)
        else:
            if val is None:
                val = ""
            info("posting fact: %s" % val)
            node = parse_and_store(val, node)
            self.write_page(node)


class RelatedNodesPage(ListPage):

    def write_page(self, val):
        words = parse(val)
        info("searching for words: %s" % words)

        first_word = words[0]
        fetched_nodes = Node.all().filter('val = ', first_word).fetch(50)
        big_grid = []
        if len(words) == 1:
            for node in fetched_nodes:
                node.val = "<b>" + node.val + "</b>"
                big_grid += node.full_grid()
        else:
            for node in fetched_nodes:
                searched_node = node.search(words[1:])
                node.val = "<b>" + node.val
                node_grid = node.full_grid()
                for row in node_grid:
                    for current_node in row:
                        if current_node.key() == searched_node.key():
                            current_node.val = current_node.val + "</b>"
                big_grid += node_grid
        template_values = {
            'val': val,
            'grid': big_grid
        }
        self.start()
        path = os.path.join(
            os.path.dirname(__file__),
            '../web/dynamic/relatednodes.html')
        self.response.out.write(template.render(path, template_values))
        self.end()

    def get(self):
        val = self.request.get('val')
        self.write_page(val)
